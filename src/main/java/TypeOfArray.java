import java.util.*;

public class TypeOfArray {

    public static int getTypeOfArray(int[] arr){

        int even=0, odd=0;

        for(int element:arr) {
            if (element % 2 == 0)
                even++;
            else
                odd++;
        }

        if(odd==0)
            return 1;
        else if(even==0)
            return 2;

        return 3;

    }

    public static void main(String[] args){

        Scanner scan= new Scanner(System.in);

        int length;
        length= scan.nextInt();

        int[] arr= new int[length];
        for(int i=0; i<length; i++)
            arr[i]= scan.nextInt();

        int type_of_array= getTypeOfArray(arr);
        String type="";

        if(type_of_array==1)
            type="Even";
        else if(type_of_array==2)
            type="Odd";
        else if(type_of_array==3)
            type="Mixed";

        System.out.println(type);

    }

}
